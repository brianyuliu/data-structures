#include <stdio.h>
#include <stdlib.h>

// size of node (singly LL) will be 16 here because 4(data) + 4(padding) + 8(node* next) = 16 bytes
typedef struct node{
	int data;
	struct node* next;
} node;

typedef struct doublyNode{
	int data;
	struct doublyNode* next;
	struct doublyNode* prev; 
} doubleNode;
