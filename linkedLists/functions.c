#include <stdio.h>
#include <stdlib.h>
#include "functions.h"

// create a new node and return pointer to that node
node* newNode(int value, node* next){
        printf("\n-={ New Node }=-");
        node* newNode = (node *)malloc(sizeof(node));
        newNode->data = value;
        newNode->next = next;
        return newNode;
}

// given the head of a LL, reverse the LL 
node* reverseList(node* head){
	printf("\n-={ Reverse List }=-\n");
	node* prev = NULL;
	node* next = NULL;
	node* curr = head;
	
	while(curr!=NULL){
		next = curr->next;
		curr->next = prev;
		prev = curr;
		curr = next;
	}
	return prev;
}

// given the head of a LL, remove the start node of LL
node* removeStart(node*head){
	printf("\n-={ removeStart }=-\n");
	node* newHead = head->next;
	free(head);
	return newHead;
}

// given the head of a LL, remove the end node of LL
node* removeEnd(node*head){
	printf("\n-={ removeEnd }=-\n");
	node* start = head;
	while(head->next->next != NULL)
		head = head->next;
	free(head->next);
	head->next = NULL;
	return start;
}

// given the head of a LL and a new node, add new node to start of LL
node* addToStart(node* head, node* new){
	printf("\n-={ addToStart }=-\n");
	new->next = head;
        return new;
}

// given the head of a LL and a new node, add new node to end of LL
node* addToEnd(node* head, node* new){
	printf("\n-={ addToEnd }=-\n");
	node* start = head;
	while(head->next != NULL)
		head = head->next;
	head->next = new;
	return start;
}

// Given the head of a linked list, remove the nth node from the end of the list
node* removeNth(node* head, int n){
	printf("\n-={ RemoveNth -> n=%d }=-\n", n);
	node* start = head;
	node* prev = NULL;
	n--; // have n start at n-1. if we want to delete nth node, we iterate/travel n-1 times
	while(start!=NULL && n){ // get to the Nth node that we want to delete
		prev = start;
		start = start->next;
		n--;
	}
	if(start == NULL){ // case where the nth node we want to delete > length of LL. start would be NULL (next of last node would be NULL)
		printf("\nNth node not in LL! (>)\n");
		return NULL;
	}
	node* next = start->next;
	free(start);
	if(prev == NULL) // case where n == first node of LL, prev would be NULL bc never assigned in while loop
		return next;
	prev->next = next; // case where nth node is in middle or end of LL
	return head;
}

// Given the head of a LL and a new node, add the new node into the LL into the Nth place
node* addNth(node* head, node* new, int n){
	printf("\n-={ AddNth -> n=%d}=-\n", n);
	node* curr = head;
	node* prev = NULL;
	n--;  // have n start at n-1. if we want to add new node to nth place, we iterate/travel n-1 times
	while(curr != NULL && n){ // get to the node right before Nth (would be prev)
		prev = curr;
		curr=curr->next;
		n--;
	}
	if(n){ // case where Nth node > length of LL
		printf("\nNth node not in LL! (>)\n");
		return NULL;
	}
	if(prev==NULL){ // case where Nth is first node in list 
		prev = new;
		prev->next = curr;
		return prev;
	}
	prev->next = new;
	prev->next->next = curr;
	return head;
}

// Given the head of a linked list, rotate the list to the right by k places.

// You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.
// 2->4->3 + 5->6->4    =   7->0->8
node* addTogether(node* n1, node* n2){
	printf("\n-={ addTogether }=-\n");
	node* start = NULL;
	node* curr = start;
	node* prev = start;

	int newDigit = 0;
	while(n1 || n2 || newDigit){
		curr = newDigit ? newNode(1, NULL) : newNode(0, NULL);
		if(!start) // if first iteration, assign start to curr so can return start node*
			start = curr;
		
		// cases for math/storing data of new node
		if(n1&&n2){ 
			if(curr->data + n1->data+n2->data >= 10) // if curr+n1+n2>=10, next node will start at 1 instead of 0
				newDigit = 1;
			else
				newDigit = 0;
			curr->data = (curr-> data + n1->data + n2->data)%10;
		}
		else if(n1){
			if(curr->data + n1->data >= 10) // if curr+n1>=10, next node will start at 1 instead of 0
                                newDigit = 1;
			else
                                newDigit = 0;
			curr->data = (curr->data + n1->data) % 10;
		}
		else if(n2){
			if(curr->data + n2->data >= 10) // if curr+n2>=10, next node will start at 1 instead of 0
                                newDigit = 1;
			else
                                newDigit = 0;
			curr->data = (curr->data + n2->data) % 10;
		}
		else // in this case, only had to make another node/iteration because a 1 carried over. so make the node w/ data=1, link it, and set newDigit back to 0
			newDigit = 0;
		if(prev) // if not first iteration, link the prev iteration's created node to current created node. after, set prev to curr for next iteration
			prev->next = curr;
		prev = curr;
		if(n1) 
			n1 = n1->next;
		if(n2)
			n2 = n2->next;
	}
	return start;
}

// given the head of two sorted LLs, merge the two lists into one sorted list
node* mergeTwo(node* n1, node* n2){
	printf("\n-={ mergeTwo }=-\n");
	node* start = NULL;
	node* curr = NULL;
	node* prev = NULL;
	while(n1 && n2){
		if(n1->data < n2->data){ // case where n1<n2
			curr = n1;
			n1 = n1->next;
		}
		else{ // case where n1>n2 OR n1=n2. if it is =, just add n2 first, and then add n1 in the next iteration
			curr = n2;
                        n2 = n2->next;
		}
		if(start == NULL) // if this is the first iteration assign start to curr so can return head of answer list
			start = curr;
		if(prev!= NULL) // if is not first iteration, assign prev iteration node's next to current node. After, new prev is curr, for next iteration
			prev->next = curr;
		prev = curr;

	}
	// at this point, if prev is null, then the function input had 1 or 2 null LLs
	if(prev == NULL){
		if(n1)
			start = n1;
		if(n2) 
			start = n2;
		return start;
	}
	// at this point, only n1 OR n2 has nodes left so just assign current node's next to first of n1 OR n2
	if(n1)
		prev->next = n1;
	if(n2)
		prev->next = n2;
	return start;
}

// given the head of a LL< swap every 2 adjacent nodes and return its head
node* swapPairs(node* head){
	printf("\n-={ swapPairs }=-\n");
	node* start = NULL;
	node* prev = NULL;
	node* curr = head;
	while(curr != NULL && curr->next != NULL){ // loop until end of LL (even length) or last one (odd length)
		node* temp = curr->next->next; 
		curr->next->next = curr;
		if(start == NULL) // if this is the first iteration, set start node so can return start of answer LL
			start = curr->next;
		curr->next = temp;
		// DOESNT WORK FOR EVEN LENGTH -> SEGFAULT TAKING BREAK
		prev = curr; // set prev for backwards link with end of previous iteration pair to start of curr iteration pair
		if(prev)
			prev->next = curr->next->next;
		curr = temp; // set curr for next iteration
	
	}
	if(curr != NULL) // only for cases of odd length. after exiting while loop, set end of last iteration pair to final node
		prev->next = curr;
	return start;
}

// Given the head of a singly linked list, group all the nodes with odd indices together followed by the nodes with even indices, and return the reordered list.
//The first node is considered odd, and the second node is even, and so on.
//Note that the relative order inside both the even and odd groups should remain as it was in the input.
//You must solve the problem in O(1) extra space complexity and O(n) time complexity

