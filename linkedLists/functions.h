#include <stdio.h>
#include <stdlib.h>
#include "linkedLists.h"

node* newNode(int value, node* next);
node* reverseList(node* head);

node* removeStart(node* head);
node* removeEnd(node* head);
node* removeNth(node* head, int n);

node* addToStart(node* head, node* new);
node* addToEnd(node* head, node* new);
node* addNth(node* head, node* new, int n);

node* addTogether(node* n1, node* n2);
node* mergeTwo(node* n1, node* n2);
node* swapPairs(node* head);

