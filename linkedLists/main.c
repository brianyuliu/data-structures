#include <stdio.h>
#include <stdlib.h>
#include "functions.h"

/*  LL defined in linkedLists.h
typedef struct node{
	int data;
	struct node* next;
} node;

typedef struct doublyNode{
	int data;
	struct doublyNode* next;
	struct doublyNode* prev;
} doubleNode;
*/

// set up a linkedlist with 5 nodes and return pointer to head node -> (0, second), (1, third), (2, fourth), (3, fifth), (4, NULL) 
node* setup(){
	printf("-={ Setup }=-");
	node* head = newNode(0, NULL);
	node* second = newNode(1, NULL);
	node* third = newNode(2, NULL);
	node* fourth = newNode(3, NULL);
	node* fifth = newNode(4, NULL);

	head->next = second;
	second->next = third;	
	third->next = fourth;	
	fourth->next = fifth;	

	return head;
}

node* setupAddTwoFirst(){
	node* head = newNode(4, NULL);
        node* second = newNode(6, NULL);
        node* third = newNode(2, NULL);
	head->next = second;
        second->next = third;
	return head;
}

node* setupAddTwoSecond(){
	node* head = newNode(7, NULL);
        node* second = newNode(9, NULL);
        node* third = newNode(0, NULL);
	node* fourth = newNode(9, NULL);
	head->next = second;
        second->next = third;
	third->next = fourth;
	return head;
}

node* setupAddTwoThird(){
        node* head = newNode(4, NULL);
        node* second = newNode(6, NULL);
        node* third = newNode(9, NULL);
        head->next = second;
        second->next = third;
        return head;
}

node* setupMergeTwoFirst(){
        node* head = newNode(1, NULL);
        node* second = newNode(3, NULL);
        node* third = newNode(5, NULL);
        head->next = second;
        second->next = third;
        return head;
}

node* setupMergeTwoSecond(){
        node* head = newNode(2, NULL);
        node* second = newNode(4, NULL);
        node* third = newNode(6, NULL);
        node* fourth = newNode(8, NULL);
        node* fifth = newNode(10, NULL);
        node* sixth = newNode(12, NULL);
        head->next = second;
        second->next = third;
        third->next = fourth;
        fourth->next = fifth;
        fifth->next = sixth;
        return head;
}

node* setupMergeTwoThird(){
        node* head = newNode(1, NULL);
        node* second = newNode(2, NULL);
        node* third = newNode(4, NULL);
        head->next = second;
        second->next = third;
        return head;
}

node* setupMergeTwoFourth(){
        node* head = newNode(1, NULL);
        node* second = newNode(3, NULL);
        node* third = newNode(4, NULL);
        head->next = second;
        second->next = third;
        return head;
}

node* setupSwapPairs(){
        printf("-={ Setup }=-");
        node* head = newNode(0, NULL);
        node* second = newNode(1, NULL);
        node* third = newNode(2, NULL);
        node* fourth = newNode(3, NULL);
        node* fifth = newNode(4, NULL);
        node* sixth = newNode(5, NULL);
        node* seventh = newNode(6, NULL);

	head->next = second;
        second->next = third;
        third->next = fourth;
        fourth->next = fifth;
        fifth->next = sixth;
	sixth->next = seventh;

        return head;
}

node* setupSwapPairsSecond(){
        printf("-={ Setup }=-");
        node* head = newNode(0, NULL);
        node* second = newNode(1, NULL);
        node* third = newNode(2, NULL);
        node* fourth = newNode(3, NULL);
        node* fifth = newNode(4, NULL);
        node* sixth = newNode(5, NULL);

        head->next = second;
        second->next = third;
        third->next = fourth;
        fourth->next = fifth;
        fifth->next = sixth;

        return head;
}


// given a node in a LL, print node until the end of the LL. will not alter the LL
void printLL(node* head){
	printf("\n-={ PRINT LL }=-\n");
	while(head){
		printf("data -> %d  // addy -> %p // next -> %p\n", head->data, head, head->next);
		head = head->next;
	}
}

int main(){
	printf("--=={{ Linked Lists }}==--\n");
	node* head = setup(); // *** malloc will allocated blocks in multiples of 32 bytes. allocating an int(4) would allocate 32
	printLL(head);

	// functions to solve LL problems
	printf("\n------------------------------------------------------\n");
	head = reverseList(head);
	printLL(head);
	printf("\n------------------------------------------------------\n");
        printLL(addToEnd(head, newNode(99, NULL)));
	printf("\n------------------------------------------------------\n");
        head = addToStart(head, newNode(98, NULL));
	printLL(head);
	printf("\n------------------------------------------------------\n");
        head = removeStart(head);
	printLL(head);
	printf("\n------------------------------------------------------\n");
        printLL(removeEnd(head));
	printf("\n------------------------------------------------------\n");
        printLL(removeNth(head, 3));
	printf("\n------------------------------------------------------\n");
        printLL(addNth(head, newNode(97, NULL), 5));
	printf("\n------------------------------------------------------\n");
	node* n1 = setupAddTwoFirst();
	node* n2 = setupAddTwoSecond();

	printLL(n1);
	printLL(n2);

	printLL(addTogether(n1, n2));
	printf("\n------------------------------------------------------\n");
	node* n3 = setupAddTwoThird();
        node* n4 = setupAddTwoSecond();

        printLL(n3);
        printLL(n4);

        printLL(addTogether(n3, n4));
        printf("\n------------------------------------------------------\n");
	node* n5 = setupMergeTwoFirst();
        node* n6 = setupMergeTwoSecond();

        printLL(n5);
        printLL(n6);

        printLL(mergeTwo(n5, n6));
        printf("\n------------------------------------------------------\n");
	node* n7 = setupMergeTwoThird();
        node* n8 = setupMergeTwoFourth();

        printLL(n7);
        printLL(n8);

        printLL(mergeTwo(n7, n8));
        printf("\n------------------------------------------------------\n");
        node* n9 = setupMergeTwoFourth();
	printLL(mergeTwo(NULL, n9));
        printf("\n------------------------------------------------------\n");
        node* n10 = setupSwapPairs();
	printLL(n10);
	printLL(swapPairs(n10));
        printf("\n------------------------------------------------------\n");
	node* n11 = setupSwapPairsSecond();
        printLL(n11);
        printLL(swapPairs(n11));
        printf("\n------------------------------------------------------\n");

}
