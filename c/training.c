#include <stdio.h> // imports the header file with the necessary information of the standard input output library

void test(int testInt ){
	printf("You entered - %d\n", testInt);
}


int main(){
	printf("Welcome to a training project!\n");
	
	int testInt;
	printf("Enter an int: ");
	scanf("%d", &testInt);
	test(testInt);
}

