# directed graphs
graphOne = [ [1], [0,1,2], [4], [2], [4] ]; # adjacency list
graphTwo = [ [0,1,0,0,0], [1,1,1,0,0], [0,0,0,0,1], [0,0,1,0,0], [0,0,0,0,1] ]; # adjacency matrix

graphThree = [ [1,2], [3,4], [5,6], [7,8], [9], [10], [11,12]] # adjacency list

graphFour = [ [[1, 6], [3, 1]], # Node A
              [[0, 6], [2, 5], [3, 2], [4,2]], # Node B
              [[1,5], [4, 5]], # Node C
              [[0,1], [1,2], [4,1]], # Node D
              [[3,1], [1,2], [2,5]] ] # Node E
# weighted adjacency list. [destination vertex, weight]

graphFive = [ [[1,4], [7,8]], # Node 0 
              [[0,4], [2,8], [7,11]], # Node 1
              [[1,8], [3,7], [5,4], [8,2]], # Node 2
              [[2,7], [4,9], [5,14]], # Node 3
              [[3,9], [5,10]], # Node 4
              [[2,4], [3,14], [4,10], [6,2]], # Node 5
              [[5,2], [7,1], [8,6]], # Node 6
              [[0,8], [1,11], [6,1], [8,7]], # Node 7
              [[2,2], [6,6], [7,7]]] # Node 8

graphSix = [ [[1,0], [2,0]], [[3,0], [4,0]], [[5,0], [6,0]], [[7,0], [8,0]], [[9,0]], [[10,0]], [[11,0], [12,0]]] # weighted adjacency list. [destination vertex, weight]


