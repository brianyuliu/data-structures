from example_graphs import *
from graph_functions import *

# print graphOne, which is an adjacency list (5x5) // format a -> b = directed edge from vertex a to vertex b
def printAL(graph):
        print("\n-={ Adjacency List }=-\n")
        for i in range(len(graph)):
                for j in range(len(graph[i])):
                        if(graph[i][j]):
                                print(str(i) + " -> " + str(graph[i][j]))                
                print("---------------------")


# print graphTwo, which is an adjacency matrix (5x5) // format a -> b = directed edge from vertex a to vertex b
def printAG(graph):
        print("\n-={ Adjacency Matrix }=-\n")
        for i in range(len(graph)):
                for j in range(len(graph[i])):
                        if(graph[i][j]):
                                print(str(i) + " -> " + str(j+1))                

                print("---------------------")
        

if __name__ == "__main__":
        print("-={ GRAPHS }=-")
        """
        printAL(graphOne)
        printAG(graphTwo)

        print("\n-={ BFS }=-\n")
        print(BFS(graphOne, 4)) # should explore 0,1,2

        print("\n-={ DFS }=-\n")
        print(DFS(graphOne, 4)) # should explore 0,1,2
        """
        #------------------------------------
        #printAL(graphThree)

        print("\n-={ BFS }=-\n")
        #print(BFS(graphThree, 7)) # should explore 0,1,2,3

        print("\n-={ DFS }=-\n")
        #print(DFS(graphThree, 7)) # should explore 0,2,6,12,11,5,10,1,4,9,3

        print("\n-={ Dijkstra's (Single Source) (graphFour) }=-\n")
        print(dijkstras(graphFour, 0)) # should print {0: (0, 0), 1: [3, 3], 2: [7, 4], 3: [1, 0], 4: [2, 3]}
        print("\n-={ Dijkstra's (Single Source) (graphFive) }=-\n")
        print(dijkstras(graphFive, 0)) # should print {0: (0, 0), 1: [4, 0], 2: [12, 1], 3: [19, 2], 4: [21, 5], 5: [11, 6], 6: [9, 7], 7: [8, 0], 8: [14, 2]}

        
        print("\n-={ Bellman-Ford (Single Source) (graphFour) }=-\n")
        print(bellmanford(graphFour, 0)) # should print {0: 0, 1: 3, 2: 7, 3: 1, 4: 2}
        print("\n-={ Bellman-Ford (Single Source) (graphFive) }=-\n")
        print(bellmanford(graphFive, 0)) # should print {0: 0, 1: 4, 2: 12, 3: 19, 4: 21, 5: 11, 6: 9, 7: 8, 8: 14}

        print("\n-={ Floyd-Warshall (All Pairs) (graphFour) }=-\n")
        print(floydwarshall(graphFour)) # should print 
        print("\n-={ Floyd-Warshall (All Pairs) (graphFive) }=-\n")
        print(floydwarshall(graphFive)) # should print 


