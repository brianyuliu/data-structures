# search - bfs, dfs
# BFS using an adjacency list
def BFS(graph: list, search:  int) -> bool:
    if not graph:
        return False
    queue = [0]
    visited = [0]
    while queue:
        print("Exploring: " + str(queue[0]))
        for i in graph[queue.pop(0)]: # get all neighbors of first node in queue
            if i not in visited:
                if i == search:
                    return True
                queue.append(i)
                visited.append(i)
        print("Queue: " + str(queue))
        print("------")

# DFS using an adjacency list      
def DFS(graph: list, visited: list, node: int, search:  int) -> bool:
    if node not in visited:
        print("Exploring: " + str(node))
        visited.add(node)
        for i in graph(node):
            dfs(graph, visited, i, search)

# shortest paths - dijkstra's, bellman-ford, floyd-warshall, A*
# Dijkstra's // single source shortest path alg // O(V^2)
def dijkstras(graph: list, source: int):
    # initialize our two data structs to track
    tracker = {i: [float('inf'), None] for i in range(len(graph))}
    unvisited = [i for i in range(len(graph))]

    # initialize our source node
    tracker[source] = (0, source)
    
    print(tracker)
    print(unvisited)
    print("----------- ^ initialized -----------")

    while unvisited:
        # visit unvisted vertex with the smallest known distance from the start vertex
        # sort the unvisited list by shortest distance from start vetex in tracker and take the first elm to visit/explore
        curr = sorted(unvisited, key = lambda k : tracker[k][0])[0] # curr = current node we are visiting
        print("Visiting: " + str(curr))
        # explore all of the current node's neighbors and calculate/compare distance
        for i in graph[curr]:
            if i[0] in unvisited: # only calculate distance if the neighbor has not been visited by alg
                calculated_distance = tracker[curr][0] + i[1] # distance of curr node + edge btwn curr and neigbor
                if calculated_distance <= tracker[i[0]][0]:
                    tracker[i[0]][0] = calculated_distance
                    tracker[i[0]][1] = curr
        
        # when done, remove the node that was just explored from unvisited list
        unvisited.remove(curr)
    return tracker
    
# Bellman-Ford // single source shortest path // O(V*E)
def bellmanford(graph: list, source: int):
    tracker = {i: float('inf') for i in range(len(graph))}
    tracker[source] = 0

    for i in range(len(graph) - 1): # iterate at most V-1 times
        for v1 in range(len(graph)): # iterate through all edges v1->v2 (v1)
            for v2 in graph[v1]: # iterate through all edges v1-> v2 (v2)
                calculated_distance = tracker[v1] + v2[1]
                if calculated_distance < tracker[v2[0]]:
                    tracker[v2[0]] = calculated_distance
                    changed = 1
        # can implement a changed variable to end outer loop early if no updates to save time

    # iterate one more time to check for negative weight cycles
    for v1 in range(len(graph)):
        for v2 in graph[v1]:
            calculated_distance = tracker[v1] + v2[1]
            if calculated_distance < tracker[v2[0]]: # if this is the case, there must be a negative weight cycle
                return "Negative Weight Cycle Detected!!"
     
    return tracker

# Floyd-Warshall // all pairs shortest path // O(V^3)
def floydwarshall(graph: list):
    tracker = [[float('inf') for i in range(len(graph))] for j in range(len(graph))] # 2d array of size V*V, with each value tracker[x][y] being distance from node x to node y

    # initiate all values [v][v] to 0 in the table (distance from every vertex to itself is 0)
    for v in range(len(graph)):
            tracker[v][v] = 0

    # add weights of all existent edges in graph and add to table
    for v1 in range(len(graph)):
            for v2 in graph[v1]:
                tracker[v1][v2[0]] = v2[1]

    # triple for loop
    for k in range(len(graph)):
        for i in range(len(graph)):
            for j in range(len(graph)):
                if tracker[i][j] > tracker[i][k] + tracker[k][j]:
                    tracker[i][j] = tracker[i][k] + tracker[k][j]
                    
    return tracker


# MST - prim's, kruskal's 
# topological sort
# strongly connected components

