#include <stdio.h>
#include <stdlib.h>
#include "algorithms.h"

// print graphOne, which is an adjacency list (5x5) // format a -> b = directed edge from vertex a to vertex b
void printAL(int graph[5][5]){
	printf("\n-={ graphOne }=-\n");
	for(int i = 0; i < 5; i++){
		for(int j = 0; j<5; j++){
			if(graph[i][j]) 
				printf("%d -> %d\n", i+1, graph[i][j]);
		}
		printf("---------------------\n");
	}
}

// print graphTwo, which is an adjacency matrix (5x5) // format a -> b = directed edge from vertex a to vertex b
void printAG(int graph[5][5]){
	printf("\n-={ graphTwo }=-\n");
	for(int i = 0; i < 5; i++){ 
                for(int j = 0; j<5; j++){
                        if(graph[i][j]) 
                                printf("%d -> %d\n", i+1, j+1);
                }
                printf("---------------------\n");
        }
}

int main(){
	printf("-={ GRAPHS }=-\n");
	printAL(graphOne);
	printAG(graphTwo);
}
